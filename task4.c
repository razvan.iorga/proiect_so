// Programul se va apela astfel:
// gcc -Wall -o p task4.c
// ./p -o outputDirectory -s quarantineDirectory -c script.sh testDirectory1 testDirectory2 [testDirectory3 ...]

#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <string.h>
#include <unistd.h>
#include <dirent.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <stdbool.h>
#include <fcntl.h>
#include <stdint.h>
#include <libgen.h>

#define PATH_MAX 4096

typedef enum {
  COMPARE_AND_REPLACE,
  WRITE
}Mode_t;

bool statFileExists(const char* statFileName) {
  int fileDescriptor = open(statFileName, O_RDWR);
  return ((fileDescriptor == -1) ? false : true);
}

int openStatFile(const char* statFileName) {
  return open(statFileName, O_RDWR);
}

int createAndOpenStatFile(const char* statFileName) {
  return open(statFileName, O_CREAT | O_RDWR, S_IRUSR | S_IWUSR);
}

bool compareAndReplaceStatFile(const char* directoryName, DIR* directoryReference, int statFileDescriptor, const char* scriptpath, const char* quarantineDirectory);

void writeStatFile(const char* directoryName, DIR* directoryReference, int statFileDescriptor, const char* scriptpath, const char* quarantineDirectory);

void parseDirectory(const char* directoryName, int statFileDescriptor, Mode_t mode, const char* scriptpath, const char* quarantineDirectory);

void printCurrentWorkingDirectory() {
  char cwd[PATH_MAX];
  if(getcwd(cwd, sizeof(cwd)) != NULL) {
    printf("Current working directory: %s\n", cwd);
  } else {
    perror("getcwd() error.\n");
    exit(EXIT_FAILURE);
  }
}

bool hasRights(struct stat fileStats) {
  if((fileStats.st_mode & S_IRUSR) || (fileStats.st_mode & S_IWUSR) || (fileStats.st_mode & S_IXUSR)) {
    return true;
  }
  return false;
}

void moveCorruptedFile(char* filepath, const char* quarantineDirectory) {
  char destinationPath[PATH_MAX];
  sprintf(destinationPath, "%s/%s", quarantineDirectory, basename(filepath));
  if(rename(filepath, destinationPath) != 0) {
    perror("Error moving the corrupted file.\n");
    exit(EXIT_FAILURE);
  } else {
    printf("Corrupted file moved successfully.\n");
  }
}

int main(int argc, const char** argv) {

  if(argc < 8 || argc > 12) {
    perror("Incorrect number of arguments.\n");
    exit(EXIT_FAILURE);
  }
  if(strcmp(*(argv + 1), "-o") != 0) {
    perror("Second argument is not -o.\n");
    exit(EXIT_FAILURE);
  }
  if(strcmp(*(argv + 3), "-s") != 0) {
    perror("Fourth argument is not -s.\n");
    exit(EXIT_FAILURE);
  }
  if(strcmp(*(argv + 5), "-c") != 0) {
    perror("Sixth argument is not -c.\n");
    exit(EXIT_FAILURE);
  }
  if(strstr(*(argv + 6), ".sh") == NULL) {
    perror("Seventh argument is not a script.\n");
    exit(EXIT_FAILURE);
  }

  printCurrentWorkingDirectory();

  if(chdir(*(argv + 2)) == -1) {
    perror("Error changing current directory.\n");
    exit(EXIT_FAILURE);
  }

  printCurrentWorkingDirectory();

  for(uint8_t i = 7 ; i < argc ; ++i) {

    pid_t processID, waitProcessID;
    int status;
    if((processID = fork()) < 0) {
      perror("fork() error.\n");
      exit(EXIT_FAILURE);
    }
    if(processID == 0) {
      char currentStatFileName[256];
      sprintf(currentStatFileName, "statData_%d.bin", i - 6);
      printf("%s\n", currentStatFileName);

      int statFileDescriptor;
      Mode_t mode;
      if(statFileExists(currentStatFileName)) {
        printf("\n Stat file exists. \n");
        statFileDescriptor = openStatFile(currentStatFileName);
        mode = COMPARE_AND_REPLACE;
      } else {
        printf("\n Stat file does not exist. No comparison will be made this time. \n");
        statFileDescriptor = createAndOpenStatFile(currentStatFileName);
        mode = WRITE;
      }
      if(statFileDescriptor == -1) {
        perror("Error opening file.\n");
        exit(EXIT_FAILURE);
      }

      parseDirectory(*(argv + i), statFileDescriptor, mode, *(argv + 6), *(argv + 4));

      if(close(statFileDescriptor) == -1) {
        perror("Error closing stat file.\n");
        exit(EXIT_FAILURE);
      }
      exit(EXIT_SUCCESS);
    } else {
      waitProcessID = wait(&status);
      if(WIFEXITED(status)) {
        printf("Child process %d terminated with PID %d and exit code %d.\n", i - 6, waitProcessID, status);
      } else {
        perror("Singularity detected. Abort all.\n");
        exit(EXIT_FAILURE);
      }
    }

  }

  return 0;
}

bool compareAndReplaceStatFile(const char* directoryName, DIR* directoryReference, int statFileDescriptor, const char* scriptpath, const char* quarantineDirectory) {

  int dangerousFilesCounter = 0;
  bool modified = false;
  struct dirent* currentEntry = NULL;
  struct stat currentStats;
  char previousEntryName[256];
  struct stat previousStats;

  while((currentEntry = readdir(directoryReference)) != NULL) {

    if(strcmp(currentEntry->d_name, ".") == 0 || strcmp(currentEntry->d_name, "..") == 0 || strcmp(currentEntry->d_name, ".DS_Store") == 0) {
      continue;
    }

    char filepath[256];
    sprintf(filepath, "%s/%s", directoryName, currentEntry->d_name);
    if(lstat(filepath, &currentStats) == -1) {
      perror("lstat() error.\n");
      exit(EXIT_FAILURE);
    }

    if(S_ISREG(currentStats.st_mode)) {
      if(!hasRights(currentStats)) {
        ++dangerousFilesCounter;
        pid_t subProcessID, waitSubProcessID;
        int status;
        if((subProcessID = fork()) < 0) {
          perror("fork() error.\n");
          exit(EXIT_FAILURE);
        }
        if(subProcessID == 0) {
          execl("/bin/bash", "bash", scriptpath, filepath, NULL);
          perror("exec() error.\n");
          exit(EXIT_FAILURE);
        } else {
          waitSubProcessID = wait(&status);
          if(WIFEXITED(status)) {
            printf("Grandchild process %d terminated with PID %d and exit code %d.\n", dangerousFilesCounter, waitSubProcessID, status);
            if(status == 0) {
              printf("The file  %s is not corrupted.\n", currentEntry->d_name);
            } else {
              printf("The file %s is corrupted and will be quarantined.\n", currentEntry->d_name);
              moveCorruptedFile(filepath, quarantineDirectory);
              continue;
            }
          } else {
            perror("Singularity detected. Abort all.\n");
            exit(EXIT_FAILURE);
          }
        }
      }
    }

    if(read(statFileDescriptor, &previousEntryName, 256) == -1) {
      perror("Read d_name error.\n");
      exit(EXIT_FAILURE);
    }
    lseek(statFileDescriptor, -256, SEEK_CUR);
    printf("Previous name: %s\n", previousEntryName);
    if(strcmp(previousEntryName, currentEntry->d_name) != 0) {
      modified = true;
    }
    printf("%s\n", currentEntry->d_name);
    if(write(statFileDescriptor, currentEntry->d_name, 256) == -1) {
      perror("Write d_name error.\n");
      exit(EXIT_FAILURE);
    }

    if(read(statFileDescriptor, &previousStats.st_mode, sizeof(currentStats.st_mode)) == -1) {
      perror("Read st_mode error.\n");
      exit(EXIT_FAILURE);
    }
    lseek(statFileDescriptor, -sizeof(currentStats.st_mode), SEEK_CUR);
    printf("Previous st_mode: %hu\n", previousStats.st_mode);
    if(previousStats.st_mode != currentStats.st_mode) {
      modified = true;
    }
    printf("%hu\n", currentStats.st_mode);
    if(write(statFileDescriptor, &currentStats.st_mode, sizeof(currentStats.st_mode)) == -1) {
      perror("Write st_mode error.\n");
      exit(EXIT_FAILURE);
    }

    if(read(statFileDescriptor, &previousStats.st_size, sizeof(currentStats.st_size)) == -1) {
      perror("Read st_size error.\n");
      exit(EXIT_FAILURE);
    }
    lseek(statFileDescriptor, -sizeof(currentStats.st_size), SEEK_CUR);
    printf("Previous st_size: %lld\n", previousStats.st_size);
    if(previousStats.st_size != currentStats.st_size) {
      modified = true;
    }
    printf("%lld\n", currentStats.st_size);
    if(write(statFileDescriptor, &currentStats.st_size, sizeof(currentStats.st_size)) == -1) {
      perror("Write st_size error.\n");
      exit(EXIT_FAILURE);
    }

    if(read(statFileDescriptor, &previousStats.st_atime, sizeof(currentStats.st_atime)) == -1) {
      perror("Read st_atime error.\n");
      exit(EXIT_FAILURE);
    }
    lseek(statFileDescriptor, -sizeof(currentStats.st_atime), SEEK_CUR);
    printf("Previous st_atime: %ld\n", previousStats.st_atime);
    if(previousStats.st_atime != currentStats.st_atime) {
      modified = true;
    }
    printf("%ld\n", currentStats.st_atime);
    if(write(statFileDescriptor, &currentStats.st_atime, sizeof(currentStats.st_atime)) == -1) {
      perror("Write st_atime error.\n");
      exit(EXIT_FAILURE);
    }

    if(read(statFileDescriptor, &previousStats.st_mtime, sizeof(currentStats.st_mtime)) == -1) {
      perror("Read st_mtime error.\n");
      exit(EXIT_FAILURE);
    }
    lseek(statFileDescriptor, -sizeof(currentStats.st_mtime), SEEK_CUR);
    printf("Previous st_mtime: %ld\n", previousStats.st_mtime);
    if(previousStats.st_mtime != currentStats.st_mtime) {
      modified = true;
    }
    printf("%ld\n", currentStats.st_mtime);
    if(write(statFileDescriptor, &currentStats.st_mtime, sizeof(currentStats.st_mtime)) == -1) {
      perror("Write st_mtime error.\n");
      exit(EXIT_FAILURE);
    }

    if(read(statFileDescriptor, &previousStats.st_ctime, sizeof(currentStats.st_ctime)) == -1) {
      perror("Read st_ctime error.\n");
      exit(EXIT_FAILURE);
    }
    lseek(statFileDescriptor, -sizeof(currentStats.st_ctime), SEEK_CUR);
    printf("Previous st_ctime: %ld\n", previousStats.st_ctime);
    if(previousStats.st_ctime != currentStats.st_ctime) {
      modified = true;
    }
    printf("%ld\n", currentStats.st_ctime);
    if(write(statFileDescriptor, &currentStats.st_ctime, sizeof(currentStats.st_ctime)) == -1) {
      perror("Write st_ctime error.\n");
      exit(EXIT_FAILURE);
    }

    printf("\n\n");

    if(S_ISDIR(currentStats.st_mode)) {
      parseDirectory(filepath, statFileDescriptor, COMPARE_AND_REPLACE, scriptpath, quarantineDirectory);
    }

  }

  return modified;

}

void writeStatFile(const char* directoryName, DIR* directoryReference, int statFileDescriptor, const char* scriptpath, const char* quarantineDirectory) {

  int dangerousFilesCounter = 0;
  struct dirent* currentEntry = NULL;
  struct stat currentStats;

  while((currentEntry = readdir(directoryReference)) != NULL) {

    if(strcmp(currentEntry->d_name, ".") == 0 || strcmp(currentEntry->d_name, "..") == 0 || strcmp(currentEntry->d_name, ".DS_Store") == 0) {
      continue;
    }

    char filepath[256];
    sprintf(filepath, "%s/%s", directoryName, currentEntry->d_name);
    if(lstat(filepath, &currentStats) == -1) {
      perror("lstat() error.\n");
      exit(EXIT_FAILURE);
    }

    if(S_ISREG(currentStats.st_mode)) {
      if(!hasRights(currentStats)) {
        ++dangerousFilesCounter;
        pid_t subProcessID, waitSubProcessID;
        int status;
        if((subProcessID = fork()) < 0) {
          perror("fork() error.\n");
          exit(EXIT_FAILURE);
        }
        if(subProcessID == 0) {
          execl("/bin/bash", "bash", scriptpath, filepath, NULL);
          perror("exec() error.\n");
          exit(EXIT_FAILURE);
        } else {
          waitSubProcessID = wait(&status);
          if(WIFEXITED(status)) {
            printf("Grandchild process %d terminated with PID %d and exit code %d.\n", dangerousFilesCounter, waitSubProcessID, status);
            if(status == 0) {
              printf("The file  %s is not corrupted.\n", currentEntry->d_name);
            } else {
              printf("The file %s is corrupted and will be quarantined.\n", currentEntry->d_name);
              moveCorruptedFile(filepath, quarantineDirectory);
              continue;
            }
          } else {
            perror("Singularity detected. Abort all.\n");
            exit(EXIT_FAILURE);
          }
        }
      }
    }

    printf("%s\n", currentEntry->d_name);
    if(write(statFileDescriptor, currentEntry->d_name, 256) == -1) {
      perror("Write d_name error.\n");
      exit(EXIT_FAILURE);
    }

    printf("%hu\n", currentStats.st_mode);
    if(write(statFileDescriptor, &currentStats.st_mode, sizeof(currentStats.st_mode)) == -1) {
      perror("Write st_mode error.\n");
      exit(EXIT_FAILURE);
    }

    printf("%lld\n", currentStats.st_size);
    if(write(statFileDescriptor, &currentStats.st_size, sizeof(currentStats.st_size)) == -1) {
      perror("Write st_size error.\n");
      exit(EXIT_FAILURE);
    }

    printf("%ld\n", currentStats.st_atime);
    if(write(statFileDescriptor, &currentStats.st_atime, sizeof(currentStats.st_atime)) == -1) {
      perror("Write st_atime error.\n");
      exit(EXIT_FAILURE);
    }

    printf("%ld\n", currentStats.st_mtime);
    if(write(statFileDescriptor, &currentStats.st_mtime, sizeof(currentStats.st_mtime)) == -1) {
      perror("Write st_mtime error.\n");
      exit(EXIT_FAILURE);
    }

    printf("%ld\n", currentStats.st_ctime);
    if(write(statFileDescriptor, &currentStats.st_ctime, sizeof(currentStats.st_ctime)) == -1) {
      perror("Write st_ctime error.\n");
      exit(EXIT_FAILURE);
    }

    printf("\n\n");

    if(S_ISDIR(currentStats.st_mode)) {
      parseDirectory(filepath, statFileDescriptor, WRITE, scriptpath, quarantineDirectory);
    }

  }

}

void parseDirectory(const char* directoryName, int statFileDescriptor, Mode_t mode, const char* scriptpath, const char* quarantineDirectory) {

  DIR* directoryReference = NULL;
  if((directoryReference = opendir(directoryName)) == NULL) {
    perror("Error opening directory.\n");
    exit(EXIT_FAILURE);
  }

  if(mode == WRITE) {
    writeStatFile(directoryName, directoryReference, statFileDescriptor, scriptpath, quarantineDirectory);
  } else {
    bool modified = compareAndReplaceStatFile(directoryName, directoryReference, statFileDescriptor, scriptpath, quarantineDirectory);
    if(modified) {
      printf("The directory %s has been modified.\n", directoryName);
    } else {
      printf("Binary success! No changes have been made to the directory %s.\n", directoryName);
    }
  }

}
