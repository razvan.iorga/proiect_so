// Programul se va apela astfel:
// gcc -Wall -o p task5.c
// ./p -o outputDirectory -s quarantineDirectory -c script.sh testDirectory1 testDirectory2 [testDirectory3 ...]
// model :
// ./p -o /Users/razvan.iorga/facultate/so/snapshots -s /Users/razvan.iorga/facultate/so/quarantine -c /Users/razvan.iorga/facultate/so/proiect/checkforcorruption.sh /Users/razvan.iorga/facultate/so/test /Users/razvan.iorga/facultate/so/test2 /Users/razvan.iorga/facultate/so/test3

#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <string.h>
#include <unistd.h>
#include <dirent.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <stdbool.h>
#include <fcntl.h>
#include <stdint.h>
#include <libgen.h>

#define PATH_MAX 4096

// tip de date enumerat pentru Snapshot
typedef enum {
  COMPARE_AND_REPLACE, // Snapshot-ul exista deja si va fi actualizat corespunzator
  WRITE // Snapshot-ul nu exista si va fi creat
}Mode_t;


// functie care verifica daca Snapshot-ul exista sau nu
bool statFileExists(const char* statFileName) {
  int fileDescriptor = open(statFileName, O_RDWR);
  return ((fileDescriptor == -1) ? false : true);
}


// functie care deschide Snapshot-ul deja creat
int openStatFile(const char* statFileName) {
  return open(statFileName, O_RDWR);
}


// functie care creeaza si deschide Snapshot-ul curent
int createAndOpenStatFile(const char* statFileName) {
  return open(statFileName, O_CREAT | O_RDWR, S_IRUSR | S_IWUSR);
}


// declaratia unei functii care va comopara si actualiza continutul unui Snapshot
bool compareAndReplaceStatFile(int* totalNumberOfEntities, const char* directoryName, DIR* directoryReference, int statFileDescriptor, const char* scriptpath, const char* quarantineDirectory);


// declaratia unei functii care va scrie pentru prima data continutul unui Snapshot
void writeStatFile(int* totalNumberOfEntities, const char* directoryName, DIR* directoryReference, int statFileDescriptor, const char* scriptpath, const char* quarantineDirectory);


// functie care va parcurge toate intrarile dintr-un director si le va adauga / actualiza in Snapshot
bool parseDirectory(int* totalNumberOfEntities, const char* directoryName, int statFileDescriptor, Mode_t mode, const char* scriptpath, const char* quarantineDirectory);


// functie care afiseaza pe ecran directorul curent de lucru
void printCurrentWorkingDirectory() {

  char cwd[PATH_MAX];

  if(getcwd(cwd, sizeof(cwd)) != NULL) {
    printf("Current working directory: %s\n", cwd);
  } else {
    perror("getcwd() error.\n");
    exit(EXIT_FAILURE);
  }

}


// functie care verifica daca un fisier are cel putin un drept (citire/scriere/executie)
bool hasRights(struct stat fileStats) {
  if((fileStats.st_mode & S_IRUSR) || (fileStats.st_mode & S_IWUSR) || (fileStats.st_mode & S_IXUSR)) {
    return true;
  }
  return false;
}

//functie care se ocupa de mutarea unui fisier corupt din directorul in care se afla in directorul special destinat acestor fisiere
void moveCorruptedFile(char* filepath, const char* quarantineDirectory) {

  // se creeaza calea catre directorul de carantina
  char destinationPath[PATH_MAX];
  sprintf(destinationPath, "%s/%s", quarantineDirectory, basename(filepath));

  // mutarea propriu-zisa a fisierului corupt
  if(rename(filepath, destinationPath) != 0) {
    perror("Error moving the corrupted file.\n");
    exit(EXIT_FAILURE);
  } else {
    printf("Corrupted file moved successfully.\n");
  }

}


int main(int argc, const char** argv) {

  // verificam ca numarul de argumente primit sa fie corect
  if(argc < 8 || argc > 12) {
    perror("Incorrect number of arguments.\n");
    exit(EXIT_FAILURE);
  }

  // verificam ca al doilea argument sa fie -o
  if(strcmp(*(argv + 1), "-o") != 0) {
    perror("Second argument is not -o.\n");
    exit(EXIT_FAILURE);
  }

  // verificam ca al patrulea argument sa fie -s
  if(strcmp(*(argv + 3), "-s") != 0) {
    perror("Fourth argument is not -s.\n");
    exit(EXIT_FAILURE);
  }

  // verificam ca al saselea argument sa fie -c
  if(strcmp(*(argv + 5), "-c") != 0) {
    perror("Sixth argument is not -c.\n");
    exit(EXIT_FAILURE);
  }

  // verificam ca al saptelea argument sa fie un script
  if(strstr(*(argv + 6), ".sh") == NULL) {
    perror("Seventh argument is not a script.\n");
    exit(EXIT_FAILURE);
  }

  // afisam directorul curent de lucru
  printCurrentWorkingDirectory();

  // schimbam directorul curent de lucru, astfel incat acesta sa fie directorul destinal Snapshot-urilor
  if(chdir(*(argv + 2)) == -1) {
    perror("Error changing current directory.\n");
    exit(EXIT_FAILURE);
  }

  // afisam din nou directorul curent de lucru, pentru a verifica daca acesta a fost schimbat corespunzator
  printCurrentWorkingDirectory();

  pid_t processID, waitProcessID;
  int status;

  // bucla in care se creeaza un proces nou pentru fiecare director primit ca parametru
  for(uint8_t i = 7 ; i < argc ; ++i) {

    // se creeaza un proces nou, folosind functia fork()
    if((processID = fork()) < 0) {
      perror("fork() error.\n");
      exit(EXIT_FAILURE);
    }

    // codul procesului copil, identificat prin PID = 0
    if(processID == 0) {

      char currentStatFileName[256];

      // se creeaza numele Snapshot-ului curent
      sprintf(currentStatFileName, "statData_%d.bin", i - 6);
      printf("%s\n", currentStatFileName);

      int statFileDescriptor;
      Mode_t mode;

      // verificam existenta Snapshot-ului curent
      if(statFileExists(currentStatFileName)) {

        //daca acesta exista, va fi deschis
        printf("\n Stat file exists. \n");
        statFileDescriptor = openStatFile(currentStatFileName);
        // setam corespunzator variabila mode
        mode = COMPARE_AND_REPLACE;

      } else {

        // daca acesta nu exista, acesta va fi creat si apoi deschis
        printf("\n Stat file does not exist. No comparison will be made this time. \n");
        statFileDescriptor = createAndOpenStatFile(currentStatFileName);
        // setam corespunzator variabila mode
        mode = WRITE;

      }

      // verificam daca Snapshot-ul curent a fost deschis cu succes
      if(statFileDescriptor == -1) {
        perror("Error opening file.\n");
        exit(EXIT_FAILURE);
      }

      int totalNumberOfEntities = 0, previousTotalNumberOfEntities;

      // apelam functia care parcurge tot continutul directorului curent
      bool modified = parseDirectory(&totalNumberOfEntities, *(argv + i), statFileDescriptor, mode, *(argv + 6), *(argv + 4));

      if(mode == COMPARE_AND_REPLACE) {

        // daca suntem in cazul in care Snapshot-ul trebuie actualizat, deplasam cursorul Snapshot-ul inainte de numarul total de entitati,
        // care va fi mereu la finalul Snapshot-ului
        lseek(statFileDescriptor, -sizeof(totalNumberOfEntities), SEEK_END);

        // citim din Snapshot numarul total de entitati anterior
        if(read(statFileDescriptor, &previousTotalNumberOfEntities, sizeof(totalNumberOfEntities)) == -1) {
          perror("Read totalNumberOfEntities error.\n");
          exit(EXIT_FAILURE);
        }

        // pentru verificare, il afisam pe ecran
        printf("Previous total number of entities: %d\n", previousTotalNumberOfEntities);

        // comparam numarul total de entitati curent cu cel citit din Snapshot si actualizam valoarea lui modified in mod corespunzator
        if(previousTotalNumberOfEntities != totalNumberOfEntities) {
          modified = true;
        }

      }

      // ne pozitionam la finalul Snapshot-ului
      lseek(statFileDescriptor, 0, SEEK_END);

      // pentru verificare, afisam pe ecram numarul total de entitati
      printf("%d\n", totalNumberOfEntities);

      // scriem in Snapshot numarul total de entitati
      if(write(statFileDescriptor, &totalNumberOfEntities, sizeof(totalNumberOfEntities)) == -1) {
        perror("Write total number of entities error.\n");
        exit(EXIT_FAILURE);
      }

      // inchidem descriptorul de fisier asociat Snapshot-ului curent
      if(close(statFileDescriptor) == -1) {
        perror("Error closing stat file.\n");
        exit(EXIT_FAILURE);
      }

      // daca suntem in cazul in care Snapshot-ul trebuie actualizat, verificam valoarea variabilei modified si afisam pe ecran un mesaj
      // corespunzator care ne spune daca directorul a fost modificat sau nu
      if(mode == COMPARE_AND_REPLACE) {
        if(modified) {
          printf("The directory %s has been modified.\n", *(argv + i));
        } else {
          printf("Binary success! No changes have been made to the directory %s.\n", *(argv + i));
        }
      }

      // finalizam executia procesului copil
      exit(EXIT_SUCCESS);

    }

  }

  // bucla in care asteptam incheierea proceselor copil
  // aici va ajunge doar procesul parinte, in urma lansarii in executie in paralel a tuturor proceselor copil
  for(int i = 7 ; i < argc ; ++i) {

    // asteptam dupa executia proceselor copil
    waitProcessID = wait(&status);

    // daca procesul copil a fost finalizat fara erori, se afiseaza un mesaj corespunzator
    if(WIFEXITED(status)) {
      printf("Child process %d terminated with PID %d and exit code %d.\n", i - 6, waitProcessID, status);
    } else {
      perror("Singularity detected. Abort all.\n");
      exit(EXIT_FAILURE);
    }

  }

  return 0;
}

// functie care va comopara si actualiza continutul unui Snapshot
bool compareAndReplaceStatFile(int* totalNumberOfEntities, const char* directoryName, DIR* directoryReference, int statFileDescriptor, const char* scriptpath, const char* quarantineDirectory) {

  int dangerousFilesCounter = 0;
  bool modified = false;
  struct dirent* currentEntry = NULL;
  struct stat currentStats;
  char previousEntryName[256];
  struct stat previousStats;

  // bucla care va parcurge fiecare entitate din directorul primit ca parametru
  while((currentEntry = readdir(directoryReference)) != NULL) {

    // se vor ignora directorul curent(.), respectiv directorul parinte(..) prin instructiunea continue
    if(strcmp(currentEntry->d_name, ".") == 0 || strcmp(currentEntry->d_name, "..") == 0 || strcmp(currentEntry->d_name, ".DS_Store") == 0) {
      continue;
    }

    // se creeaza calea catre intrarea curenta in variabila filepath
    char filepath[256];
    sprintf(filepath, "%s/%s", directoryName, currentEntry->d_name);

    // aplicam functia lstat() pe intrarea curenta, in urma careia vom obtine informatii esentiale despre intrarea curents
    if(lstat(filepath, &currentStats) == -1) {
      perror("lstat() error.\n");
      exit(EXIT_FAILURE);
    }

    // daca intrarea curenta este un fisier obisnuit sau o legatura simbolica, aceasta este eligibila pentru coruptie
    if(S_ISREG(currentStats.st_mode) || S_ISLNK(currentStats.st_mode)) {

      // verificam daca intrarea curenta are cel putin un drept
      if(!hasRights(currentStats)) {

        // in cazul in care intrarea curenta nu are niciun drept, incrementam variabila care contorizeaza fisierele potential corupte
        ++dangerousFilesCounter;

        pid_t pipeFileDescriptors[2];
        pid_t subProcessID, waitSubProcessID;
        int status;

        // se creeaza un pipe care va fi utilizat pentru comunicarea intre procesul copil curent si procesul nepot
        // responsabil de executia script-ului care verifica daca fisierul este corupt sau nu
        if(pipe(pipeFileDescriptors) < 0) {
          perror("Error creating pipe.\n");
          exit(EXIT_FAILURE);
        }

        // se creeaza un proces nepot responsabil de lansarea in executie a script-ului
        if((subProcessID = fork()) < 0) {
          perror("fork() error.\n");
          exit(EXIT_FAILURE);
        }

        // codul procesului nepot
        if(subProcessID == 0) {

          // se inchide capatul de citire al pipe-ului, deoarece acest proces va utiliza capatul de scriere
          if(close(pipeFileDescriptors[0]) != 0) {
            perror("Error closing pipe reading stream.\n");
            exit(EXIT_FAILURE);
          }

          // se redirecteaza iesirea standard catre capatul de scriere al pipe-ului
          if(dup2(pipeFileDescriptors[1], 1) == -1) {
            perror("dup2() error.\n");
            exit(EXIT_FAILURE);
          }

          // se lanseaza in executie script-ul primit ca argument
          execl("/bin/bash", "bash", scriptpath, filepath, NULL);

          // daca procesul nepot a ajuns aici, inseamna ca au aparut erori la executia functiei execl()
          perror("exec() error.\n");
          exit(EXIT_FAILURE);

        } else {
          // codul procesului copil (parintele procesului nepot)

          // se inchide capatul de scriere al pipe-ului, deoarece acest proces va utiliza capatul de citire
          if(close(pipeFileDescriptors[1]) != 0) {
            perror("Error closing pipe writing stream.\n");
            exit(EXIT_FAILURE);
          }

          // variabila in care se va citi continutul din pipe (SAFE sau MOVE)
          char verdict[5];

            // citim din pipe in variabila verdict
          if(read(pipeFileDescriptors[0], verdict, 4) == -1) {
            perror("Error reading from pipe.\n");
            exit(EXIT_FAILURE);
          }

          // adaugam manual terminatorul de sir de caractere
          verdict[4] = '\0';

          //inchidem capatul de citire din pipe, deoarece nu il mai folosim
          if(close(pipeFileDescriptors[0]) != 0) {
            perror("Error closing pipe reading stream.\n");
            exit(EXIT_FAILURE);
          }

          // asteptam finalizarea procesului nepot
          waitSubProcessID = wait(&status);

          // daca procesul nepot a fost finalizat cu succes, afisam un mesaj corespunzator
          if(WIFEXITED(status)) {

            printf("Grandchild process %d terminated with PID %d and exit code %d.\n", dangerousFilesCounter, waitSubProcessID, status);

            // verificam continutul citit din pipe
            if(strcmp(verdict, "SAFE") == 0) {
              // in cazul in care s-a citit SAFE, fisierul nu este corupt si se afiseaza un mesaj corespunzator
              printf("The file  %s is not corrupted.\n", currentEntry->d_name);
            } else {
              // in cazul in care s-a citit MOVE, fisierul este corupt si va fi mutat in directorul special destinal acestor fisiere
              // de asemena, se va sari la urmatoarea intrare prin instructiunea continue
              printf("The file %s is corrupted and will be quarantined.\n", currentEntry->d_name);
              moveCorruptedFile(filepath, quarantineDirectory);
              continue;
            }

          } else {
            perror("Singularity detected. Abort all.\n");
            exit(EXIT_FAILURE);
          }

        }

      }

    }

    // incrementam variabila care contorizeaza numarul de enitati din directorul primit ca parametru
    *totalNumberOfEntities = *totalNumberOfEntities + 1;

    // citim din Snapshot numele precedent al intrarii curente
    if(read(statFileDescriptor, &previousEntryName, 256) == -1) {
      perror("Read d_name error.\n");
      exit(EXIT_FAILURE);
    }

    // deplasam corespunzator cursorul din Snapshot, pentru a fi pregatit pentru suprascriere
    lseek(statFileDescriptor, -256, SEEK_CUR);

    // afisam numele precedent al intrarii curente din directorul primit ca parametru
    printf("Previous name: %s\n", previousEntryName);

    // comparam numele citit din Snapshot cu cel actual si actualizam corespunzator valoarea variabilei modified
    if(strcmp(previousEntryName, currentEntry->d_name) != 0) {
      modified = true;
    }

    // afisam numele intrarii curente din directorul primit ca parametru
    printf("%s\n", currentEntry->d_name);

    // suprascriem in Snapshot numele intrarii curente
    if(write(statFileDescriptor, currentEntry->d_name, 256) == -1) {
      perror("Write d_name error.\n");
      exit(EXIT_FAILURE);
    }

    // citim din Snapshot valoarea precedenta a lui st_mode pentru intrarea curenta
    if(read(statFileDescriptor, &previousStats.st_mode, sizeof(currentStats.st_mode)) == -1) {
      perror("Read st_mode error.\n");
      exit(EXIT_FAILURE);
    }

    // deplasam corespunzator cursorul din Snapshot, pentru a fi pregatit pentru suprascriere
    lseek(statFileDescriptor, -sizeof(currentStats.st_mode), SEEK_CUR);

    // afisam valoarea precedenta a lui st_mode pentru intrarea curenta
    printf("Previous st_mode: %hu\n", previousStats.st_mode);

    // comparam valoarea citita din Snapshot pentru st_mode cu cea actuala si actualizam corespunzator valoarea variabilei modified
    if(previousStats.st_mode != currentStats.st_mode) {
      modified = true;
    }

    // afisam valoarea lui st_mode pentru intrarea curenta
    printf("%hu\n", currentStats.st_mode);

    // suprascriem in Snapshot valoarea lui st_mode pentru intrarea curenta
    if(write(statFileDescriptor, &currentStats.st_mode, sizeof(currentStats.st_mode)) == -1) {
      perror("Write st_mode error.\n");
      exit(EXIT_FAILURE);
    }

    // citim din Snapshot valoarea precedenta a lui st_size pentru intrarea curenta
    if(read(statFileDescriptor, &previousStats.st_size, sizeof(currentStats.st_size)) == -1) {
      perror("Read st_size error.\n");
      exit(EXIT_FAILURE);
    }

    // deplasam corespunzator cursorul din Snapshot, pentru a fi pregatit pentru suprascriere
    lseek(statFileDescriptor, -sizeof(currentStats.st_size), SEEK_CUR);

    // afisam valoarea precedenta a lui st_size pentru intrarea curenta
    printf("Previous st_size: %lld\n", previousStats.st_size);

    // comparam valoarea citita din Snapshot pentru st_size cu cea actuala si actualizam corespunzator valoarea variabilei modified
    if(previousStats.st_size != currentStats.st_size) {
      modified = true;
    }

    // afisam valoarea lui st_size pentru intrarea curenta
    printf("%lld\n", currentStats.st_size);

    // suprascriem in Snapshot valoarea lui st_size pentru intrarea curenta
    if(write(statFileDescriptor, &currentStats.st_size, sizeof(currentStats.st_size)) == -1) {
      perror("Write st_size error.\n");
      exit(EXIT_FAILURE);
    }

    // citim din Snapshot valoarea precedenta a lui st_atime pentru intrarea curenta
    if(read(statFileDescriptor, &previousStats.st_atime, sizeof(currentStats.st_atime)) == -1) {
      perror("Read st_atime error.\n");
      exit(EXIT_FAILURE);
    }

    // deplasam corespunzator cursorul din Snapshot, pentru a fi pregatit pentru suprascriere
    lseek(statFileDescriptor, -sizeof(currentStats.st_atime), SEEK_CUR);

    // afisam valoarea precedenta a lui st_atime pentru intrarea curenta
    printf("Previous st_atime: %ld\n", previousStats.st_atime);

    // comparam valoarea citita din Snapshot pentru st_atime cu cea actuala si actualizam corespunzator valoarea variabilei modified
    if(previousStats.st_atime != currentStats.st_atime) {
      modified = true;
    }

    // afisam valoarea lui st_atime pentru intrarea curenta
    printf("%ld\n", currentStats.st_atime);

    // suprascriem in Snapshot valoarea lui st_atime pentru intrarea curenta
    if(write(statFileDescriptor, &currentStats.st_atime, sizeof(currentStats.st_atime)) == -1) {
      perror("Write st_atime error.\n");
      exit(EXIT_FAILURE);
    }

    // citim din Snapshot valoarea precedenta a lui st_mtime pentru intrarea curenta
    if(read(statFileDescriptor, &previousStats.st_mtime, sizeof(currentStats.st_mtime)) == -1) {
      perror("Read st_mtime error.\n");
      exit(EXIT_FAILURE);
    }

    // deplasam corespunzator cursorul din Snapshot, pentru a fi pregatit pentru suprascriere
    lseek(statFileDescriptor, -sizeof(currentStats.st_mtime), SEEK_CUR);

    // afisam valoarea precedenta a lui st_mtime pentru intrarea curenta
    printf("Previous st_mtime: %ld\n", previousStats.st_mtime);

    // comparam valoarea citita din Snapshot pentru st_mtime cu cea actuala si actualizam corespunzator valoarea variabilei modified
    if(previousStats.st_mtime != currentStats.st_mtime) {
      modified = true;
    }

    // afisam valoarea lui st_mtime pentru intrarea curenta
    printf("%ld\n", currentStats.st_mtime);

    // suprascriem in Snapshot valoarea lui st_mtime pentru intrarea curenta
    if(write(statFileDescriptor, &currentStats.st_mtime, sizeof(currentStats.st_mtime)) == -1) {
      perror("Write st_mtime error.\n");
      exit(EXIT_FAILURE);
    }

    // citim din Snapshot valoarea precedenta a lui st_ctime pentru intrarea curenta
    if(read(statFileDescriptor, &previousStats.st_ctime, sizeof(currentStats.st_ctime)) == -1) {
      perror("Read st_ctime error.\n");
      exit(EXIT_FAILURE);
    }

    // deplasam corespunzator cursorul din Snapshot, pentru a fi pregatit pentru suprascriere
    lseek(statFileDescriptor, -sizeof(currentStats.st_ctime), SEEK_CUR);

    // afisam valoarea precedenta a lui st_ctime pentru intrarea curenta
    printf("Previous st_ctime: %ld\n", previousStats.st_ctime);

    // comparam valoarea citita din Snapshot pentru st_ctime cu cea actuala si actualizam corespunzator valoarea variabilei modified
    if(previousStats.st_ctime != currentStats.st_ctime) {
      modified = true;
    }

    // afisam valoarea lui st_ctime pentru intrarea curenta
    printf("%ld\n", currentStats.st_ctime);

    // suprascriem in Snapshot valoarea lui st_ctime pentru intrarea curenta
    if(write(statFileDescriptor, &currentStats.st_ctime, sizeof(currentStats.st_ctime)) == -1) {
      perror("Write st_ctime error.\n");
      exit(EXIT_FAILURE);
    }

    printf("\n\n");

    // in cazul in care intrarea curenta este un director, apelam recursiv functia parseDirectory() pentru intrarea curenta
    if(S_ISDIR(currentStats.st_mode)) {
      parseDirectory(totalNumberOfEntities, filepath, statFileDescriptor, COMPARE_AND_REPLACE, scriptpath, quarantineDirectory);
    }

  }

  // returnam valoarea variabilei modified
  return modified;

}

//functie care va scrie pentru prima data continutul unui Snapshot
void writeStatFile(int* totalNumberOfEntities, const char* directoryName, DIR* directoryReference, int statFileDescriptor, const char* scriptpath, const char* quarantineDirectory) {

  int dangerousFilesCounter = 0;
  struct dirent* currentEntry = NULL;
  struct stat currentStats;

  // bucla care va parcurge fiecare entitate din directorul primit ca parametru
  while((currentEntry = readdir(directoryReference)) != NULL) {

    // se vor ignora directorul curent(.), respectiv directorul parinte(..) prin instructiunea continue
    if(strcmp(currentEntry->d_name, ".") == 0 || strcmp(currentEntry->d_name, "..") == 0 || strcmp(currentEntry->d_name, ".DS_Store") == 0) {
      continue;
    }

    // se creeaza calea catre intrarea curenta in variabila filepath
    char filepath[256];
    sprintf(filepath, "%s/%s", directoryName, currentEntry->d_name);

    // aplicam functia lstat() pe intrarea curenta, in urma careia vom obtine informatii esentiale despre intrarea curenta
    if(lstat(filepath, &currentStats) == -1) {
      perror("lstat() error.\n");
      exit(EXIT_FAILURE);
    }

    // daca intrarea curenta este un fisier obisnuit sau o legatura simbolica, aceasta este eligibila pentru coruptie
    if(S_ISREG(currentStats.st_mode) || S_ISLNK(currentStats.st_mode)) {

      // verificam daca intrarea curenta are cel putin un drept
      if(!hasRights(currentStats)) {

        // in cazul in care intrarea curenta nu are niciun drept, incrementam variabila care contorizeaza fisierele potential corupte
        ++dangerousFilesCounter;

        pid_t pipeFileDescriptors[2];
        pid_t subProcessID, waitSubProcessID;
        int status;

        // se creeaza un pipe care va fi utilizat pentru comunicarea intre procesul copil curent si procesul nepot
        // responsabil de executia script-ului care verifica daca fisierul este corupt sau nu
        if(pipe(pipeFileDescriptors) < 0) {
          perror("Error creating pipe.\n");
          exit(EXIT_FAILURE);
        }

        // se creeaza un proces nepot responsabil de lansarea in executie a script-ului
        if((subProcessID = fork()) < 0) {
          perror("fork() error.\n");
          exit(EXIT_FAILURE);
        }

        // codul procesului nepot
        if(subProcessID == 0) {

          // se inchide capatul de citire al pipe-ului, deoarece acest proces va utiliza capatul de scriere
          if(close(pipeFileDescriptors[0]) != 0) {
            perror("Error closing pipe reading stream.\n");
            exit(EXIT_FAILURE);
          }

          // se redirecteaza iesirea standard catre capatul de scriere al pipe-ului
          if(dup2(pipeFileDescriptors[1], 1) == -1) {
            perror("dup2() error.\n");
            exit(EXIT_FAILURE);
          }

          // se lanseaza in executie script-ul primit ca argument
          execl("/bin/bash", "bash", scriptpath, filepath, NULL);

          // daca procesul nepot a ajuns aici, inseamna ca au aparut erori la executia functiei execl()
          perror("exec() error.\n");
          exit(EXIT_FAILURE);

        } else {
          // codul procesului copil (parintele procesului nepot)

          // se inchide capatul de scriere al pipe-ului, deoarece acest proces va utiliza capatul de citire
          if(close(pipeFileDescriptors[1]) != 0) {
            perror("Error closing pipe writing stream.\n");
            exit(EXIT_FAILURE);
          }

          // variabila in care se va citi continutul din pipe (SAFE sau MOVE)
          char verdict[5];

          // citim din pipe in variabila verdict
          if(read(pipeFileDescriptors[0], verdict, 4) == -1) {
            perror("Error reading from pipe.\n");
            exit(EXIT_FAILURE);
          }

          // adaugam manual terminatorul de sir de caractere
          verdict[4] = '\0';

          //inchidem capatul de citire din pipe, deoarece nu il mai folosim
          if(close(pipeFileDescriptors[0]) != 0) {
            perror("Error closing pipe reading stream.\n");
            exit(EXIT_FAILURE);
          }

          // asteptam finalizarea procesului nepot
          waitSubProcessID = wait(&status);

          // daca procesul nepot a fost finalizat cu succes, afisam un mesaj corespunzator
          if(WIFEXITED(status)) {

            printf("Grandchild process %d terminated with PID %d and exit code %d.\n", dangerousFilesCounter, waitSubProcessID, status);

            // verificam continutul citit din pipe
            if(strcmp(verdict, "SAFE") == 0) {
              // in cazul in care s-a citit SAFE, fisierul nu este corupt si se afiseaza un mesaj corespunzator
              printf("The file  %s is not corrupted.\n", currentEntry->d_name);
            } else {
              // in cazul in care s-a citit MOVE, fisierul este corupt si va fi mutat in directorul special destinal acestor fisiere
              // de asemena, se va sari la urmatoarea intrare prin instructiunea continue
              printf("The file %s is corrupted and will be quarantined.\n", currentEntry->d_name);
              moveCorruptedFile(filepath, quarantineDirectory);
              continue;
            }

          } else {
            perror("Singularity detected. Abort all.\n");
            exit(EXIT_FAILURE);
          }

        }

      }

    }

    // incrementam variabila care contorizeaza numarul de enitati din directorul primit ca parametru
    *totalNumberOfEntities = *totalNumberOfEntities + 1;

    // afisam numele intrarii curente din directorul primit ca parametru
    printf("%s\n", currentEntry->d_name);

    // scriem in Snapshot numele intrarii curente
    if(write(statFileDescriptor, currentEntry->d_name, 256) == -1) {
      perror("Write d_name error.\n");
      exit(EXIT_FAILURE);
    }

    // afisam continutul lui st_mode pentru intrarea curenta din directorul primit ca parametru
    printf("%hu\n", currentStats.st_mode);

    // scriem in Snapshot continutul lui st_mode pentru intrarea curenta
    if(write(statFileDescriptor, &currentStats.st_mode, sizeof(currentStats.st_mode)) == -1) {
      perror("Write st_mode error.\n");
      exit(EXIT_FAILURE);
    }

    // afisam continutul lui st_size pentru intrarea curenta din directorul primit ca parametru
    printf("%lld\n", currentStats.st_size);

    // scriem in Snapshot continutul lui st_size pentru intrarea curenta
    if(write(statFileDescriptor, &currentStats.st_size, sizeof(currentStats.st_size)) == -1) {
      perror("Write st_size error.\n");
      exit(EXIT_FAILURE);
    }

    // afisam continutul lui st_atime pentru intrarea curenta din directorul primit ca parametru
    printf("%ld\n", currentStats.st_atime);

    // scriem in Snapshot continutul lui st_atime pentru intrarea curenta
    if(write(statFileDescriptor, &currentStats.st_atime, sizeof(currentStats.st_atime)) == -1) {
      perror("Write st_atime error.\n");
      exit(EXIT_FAILURE);
    }

    // afisam continutul lui st_mtime pentru intrarea curenta din directorul primit ca parametru
    printf("%ld\n", currentStats.st_mtime);

    // scriem in Snapshot continutul lui st_mtime pentru intrarea curenta
    if(write(statFileDescriptor, &currentStats.st_mtime, sizeof(currentStats.st_mtime)) == -1) {
      perror("Write st_mtime error.\n");
      exit(EXIT_FAILURE);
    }

    // afisam continutul lui st_ctime pentru intrarea curenta din directorul primit ca parametru
    printf("%ld\n", currentStats.st_ctime);

    // scriem in Snapshot continutul lui st_ctime pentru intrarea curenta
    if(write(statFileDescriptor, &currentStats.st_ctime, sizeof(currentStats.st_ctime)) == -1) {
      perror("Write st_ctime error.\n");
      exit(EXIT_FAILURE);
    }

    printf("\n\n");

    // in cazul in care intrarea curenta este un director, apelam recursiv functia parseDirectory() pentru intrarea curenta
    if(S_ISDIR(currentStats.st_mode)) {
      parseDirectory(totalNumberOfEntities, filepath, statFileDescriptor, WRITE, scriptpath, quarantineDirectory);
    }

  }

}

// functie care parcurge toate intrarile dintr-un director si le va adauga / actualiza in Snapshot
bool parseDirectory(int* totalNumberOfEntities, const char* directoryName, int statFileDescriptor, Mode_t mode, const char* scriptpath, const char* quarantineDirectory) {

  bool modified = false;
  DIR* directoryReference = NULL;

  // deschidem directorul primit ca parametru
  if((directoryReference = opendir(directoryName)) == NULL) {
    perror("Error opening directory.\n");
    exit(EXIT_FAILURE);
  }

  // in functie de modul in care ne aflam, se va apela una din functiile care se ocupa de scrierea / actualizarea Snapshot-ului
  if(mode == WRITE) {
    writeStatFile(totalNumberOfEntities, directoryName, directoryReference, statFileDescriptor, scriptpath, quarantineDirectory);
  } else {
    modified = compareAndReplaceStatFile(totalNumberOfEntities, directoryName, directoryReference, statFileDescriptor, scriptpath, quarantineDirectory);
  }

  // inchidem directorul primit ca parametru
  if(closedir(directoryReference) != 0) {
    perror("Error closing directory.\n");
    exit(EXIT_FAILURE);
  }

  // returnam valoarea variabilei modified, care ne va spune daca Snapshot-ul a fost modificat sau nu
  return modified;

}
