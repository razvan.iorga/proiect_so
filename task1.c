// Programul se va apela astfel:
// gcc -Wall -o p task1.c
// ./p -o statData.bin testDirectory

#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <string.h>
#include <unistd.h>
#include <dirent.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <stdbool.h>
#include <fcntl.h>
#include <stdint.h>

#define PATH_MAX 4096

typedef enum {
  COMPARE_AND_REPLACE,
  WRITE
}Mode_t;

bool statFileExists(const char* statFileName) {
  int fileDescriptor = open(statFileName, O_RDWR);
  return ((fileDescriptor == -1) ? false : true);
}

int openStatFile(const char* statFileName) {
  return open(statFileName, O_RDWR);
}

int createAndOpenStatFile(const char* statFileName) {
  return open(statFileName, O_CREAT | O_RDWR, S_IRUSR | S_IWUSR);
}

bool compareAndReplaceStatFile(DIR* directoryReference, const char* statFileName, int statFileDescriptor);

void writeStatFile(DIR* directoryReference, const char* statFileName, int statFileDescriptor);

void parseDirectory(const char* directoryName, const char* statFileName);


int main(int argc, const char** argv) {

  if(argc != 4) {
    perror("Incorrect number of arguments.\n");
    exit(EXIT_FAILURE);
  }
  if(strcmp(*(argv + 1), "-o") != 0) {
    perror("Second argument is not -o.\n");
    exit(EXIT_FAILURE);
  }
  if(strstr(*(argv + 2), ".bin") == NULL) {
    perror("Third argument does not have a valid snapshot format.\n");
    exit(EXIT_FAILURE);
  }

  char cwd[PATH_MAX];
  if(getcwd(cwd, sizeof(cwd)) != NULL) {
    printf("Current working directory: %s\n", cwd);
  } else {
    perror("getcwd() error.\n");
    exit(EXIT_FAILURE);
  }

  parseDirectory(*(argv + 3), *(argv + 2));

  return 0;
}

bool compareAndReplaceStatFile(DIR* directoryReference, const char* statFileName, int statFileDescriptor) {

  bool modified = false;
  uint16_t numberOfEntities = 0;
  struct dirent* currentEntry = NULL;
  struct stat currentStats;
  char previousEntryName[256];
  struct stat previousStats;

  while((currentEntry = readdir(directoryReference)) != NULL) {

    if(strcmp(currentEntry->d_name, ".") == 0 || strcmp(currentEntry->d_name, "..") == 0 || strcmp(currentEntry->d_name, statFileName) == 0 || strcmp(currentEntry->d_name, ".DS_Store") == 0) {
      continue;
    }

    ++numberOfEntities;

    if(lstat(currentEntry->d_name, &currentStats) == -1) {
      perror("lstat() error.\n");
      exit(EXIT_FAILURE);
    }

    if(read(statFileDescriptor, &previousEntryName, 256) == -1) {
      perror("Read d_name error.\n");
      exit(EXIT_FAILURE);
    }
    lseek(statFileDescriptor, -256, SEEK_CUR);
    printf("Previous name: %s\n", previousEntryName);
    if(strcmp(previousEntryName, currentEntry->d_name) != 0) {
      modified = true;
    }
    printf("%s\n", currentEntry->d_name);
    if(write(statFileDescriptor, currentEntry->d_name, 256) == -1) {
      perror("Write d_name error.\n");
      exit(EXIT_FAILURE);
    }

    if(read(statFileDescriptor, &previousStats.st_mode, sizeof(currentStats.st_mode)) == -1) {
      perror("Read st_mode error.\n");
      exit(EXIT_FAILURE);
    }
    lseek(statFileDescriptor, -sizeof(currentStats.st_mode), SEEK_CUR);
    printf("Previous st_mode: %hu\n", previousStats.st_mode);
    if(previousStats.st_mode != currentStats.st_mode) {
      modified = true;
    }
    printf("%hu\n", currentStats.st_mode);
    if(write(statFileDescriptor, &currentStats.st_mode, sizeof(currentStats.st_mode)) == -1) {
      perror("Write st_mode error.\n");
      exit(EXIT_FAILURE);
    }

    if(read(statFileDescriptor, &previousStats.st_size, sizeof(currentStats.st_size)) == -1) {
      perror("Read st_size error.\n");
      exit(EXIT_FAILURE);
    }
    lseek(statFileDescriptor, -sizeof(currentStats.st_size), SEEK_CUR);
    printf("Previous st_size: %lld\n", previousStats.st_size);
    if(previousStats.st_size != currentStats.st_size) {
      modified = true;
    }
    printf("%lld\n", currentStats.st_size);
    if(write(statFileDescriptor, &currentStats.st_size, sizeof(currentStats.st_size)) == -1) {
      perror("Write st_size error.\n");
      exit(EXIT_FAILURE);
    }

    if(read(statFileDescriptor, &previousStats.st_atime, sizeof(currentStats.st_atime)) == -1) {
      perror("Read st_atime error.\n");
      exit(EXIT_FAILURE);
    }
    lseek(statFileDescriptor, -sizeof(currentStats.st_atime), SEEK_CUR);
    printf("Previous st_atime: %ld\n", previousStats.st_atime);
    if(previousStats.st_atime != currentStats.st_atime) {
      modified = true;
    }
    printf("%ld\n", currentStats.st_atime);
    if(write(statFileDescriptor, &currentStats.st_atime, sizeof(currentStats.st_atime)) == -1) {
      perror("Write st_atime error.\n");
      exit(EXIT_FAILURE);
    }

    if(read(statFileDescriptor, &previousStats.st_mtime, sizeof(currentStats.st_mtime)) == -1) {
      perror("Read st_mtime error.\n");
      exit(EXIT_FAILURE);
    }
    lseek(statFileDescriptor, -sizeof(currentStats.st_mtime), SEEK_CUR);
    printf("Previous st_mtime: %ld\n", previousStats.st_mtime);
    if(previousStats.st_mtime != currentStats.st_mtime) {
      modified = true;
    }
    printf("%ld\n", currentStats.st_mtime);
    if(write(statFileDescriptor, &currentStats.st_mtime, sizeof(currentStats.st_mtime)) == -1) {
      perror("Write st_mtime error.\n");
      exit(EXIT_FAILURE);
    }

    if(read(statFileDescriptor, &previousStats.st_ctime, sizeof(currentStats.st_ctime)) == -1) {
      perror("Read st_ctime error.\n");
      exit(EXIT_FAILURE);
    }
    lseek(statFileDescriptor, -sizeof(currentStats.st_ctime), SEEK_CUR);
    printf("Previous st_ctime: %ld\n", previousStats.st_ctime);
    if(previousStats.st_ctime != currentStats.st_ctime) {
      modified = true;
    }
    printf("%ld\n", currentStats.st_ctime);
    if(write(statFileDescriptor, &currentStats.st_ctime, sizeof(currentStats.st_ctime)) == -1) {
      perror("Write st_ctime error.\n");
      exit(EXIT_FAILURE);
    }

    printf("\n\n");

    if(S_ISDIR(currentStats.st_mode)) {
      parseDirectory(currentEntry->d_name, statFileName);
    }

  }

  lseek(statFileDescriptor, -sizeof(uint16_t), SEEK_END);
  uint16_t previousNumberOfEntities;
  if(read(statFileDescriptor, &previousNumberOfEntities, sizeof(uint16_t)) == -1) {
    perror("Read total number of entities error.\n");
    exit(EXIT_FAILURE);
  }
  printf("Previous number of entities: %hu\n", previousNumberOfEntities);
  if(previousNumberOfEntities != numberOfEntities) {
    modified = true;
  }
  printf("%hu\n", numberOfEntities);
  if(write(statFileDescriptor, &numberOfEntities, sizeof(uint16_t)) == -1) {
    perror("Write total number of entities error.\n");
    exit(EXIT_FAILURE);
  }

  return modified;

}

void writeStatFile(DIR* directoryReference, const char* statFileName, int statFileDescriptor) {

  uint16_t numberOfEntities = 0;
  struct dirent* currentEntry = NULL;
  struct stat currentStats;

  while((currentEntry = readdir(directoryReference)) != NULL) {

    if(strcmp(currentEntry->d_name, ".") == 0 || strcmp(currentEntry->d_name, "..") == 0 || strcmp(currentEntry->d_name, statFileName) == 0 || strcmp(currentEntry->d_name, ".DS_Store") == 0) {
      continue;
    }

    ++numberOfEntities;

    if(lstat(currentEntry->d_name, &currentStats) == -1) {
      perror("lstat() error.\n");
      exit(EXIT_FAILURE);
    }

    printf("%s\n", currentEntry->d_name);
    if(write(statFileDescriptor, currentEntry->d_name, 256) == -1) {
      perror("Write d_name error.\n");
      exit(EXIT_FAILURE);
    }

    printf("%hu\n", currentStats.st_mode);
    if(write(statFileDescriptor, &currentStats.st_mode, sizeof(currentStats.st_mode)) == -1) {
      perror("Write st_mode error.\n");
      exit(EXIT_FAILURE);
    }

    printf("%lld\n", currentStats.st_size);
    if(write(statFileDescriptor, &currentStats.st_size, sizeof(currentStats.st_size)) == -1) {
      perror("Write st_size error.\n");
      exit(EXIT_FAILURE);
    }

    printf("%ld\n", currentStats.st_atime);
    if(write(statFileDescriptor, &currentStats.st_atime, sizeof(currentStats.st_atime)) == -1) {
      perror("Write st_atime error.\n");
      exit(EXIT_FAILURE);
    }

    printf("%ld\n", currentStats.st_mtime);
    if(write(statFileDescriptor, &currentStats.st_mtime, sizeof(currentStats.st_mtime)) == -1) {
      perror("Write st_mtime error.\n");
      exit(EXIT_FAILURE);
    }

    printf("%ld\n", currentStats.st_ctime);
    if(write(statFileDescriptor, &currentStats.st_ctime, sizeof(currentStats.st_ctime)) == -1) {
      perror("Write st_ctime error.\n");
      exit(EXIT_FAILURE);
    }

    printf("\n\n");

    if(S_ISDIR(currentStats.st_mode)) {
      parseDirectory(currentEntry->d_name, statFileName);
    }

  }

  lseek(statFileDescriptor, 0, SEEK_END);
  printf("%hu\n", numberOfEntities);
  if(write(statFileDescriptor, &numberOfEntities, sizeof(uint16_t)) == -1) {
    perror("Write total number of entities error.\n");
    exit(EXIT_FAILURE);
  }

}

void parseDirectory(const char* directoryName, const char* statFileName) {

  DIR* directoryReference = NULL;
  if((directoryReference = opendir(directoryName)) == NULL) {
    perror("Error opening directory.\n");
    exit(EXIT_FAILURE);
  }

  if(chdir(directoryName) == -1) {
    perror("Error changing current directory.\n");
    exit(EXIT_FAILURE);
  }

  int statFileDescriptor;
  Mode_t mode;
  if(statFileExists(statFileName)) {
    printf("\n Stat file exists. \n");
    statFileDescriptor = openStatFile(statFileName);
    mode = COMPARE_AND_REPLACE;
  } else {
    printf("\n Stat file does not exist. No comparison will be made this time. \n");
    statFileDescriptor = createAndOpenStatFile(statFileName);
    mode = WRITE;
  }
  if(statFileDescriptor == -1) {
    perror("Error opening file.\n");
    exit(EXIT_FAILURE);
  }

  if(mode == WRITE) {
    writeStatFile(directoryReference, statFileName, statFileDescriptor);
  } else {
    bool modified = compareAndReplaceStatFile(directoryReference, statFileName, statFileDescriptor);
    if(modified) {
      printf("The directory %s has been modified.\n", directoryName);
    } else {
      printf("Binary success! No changes have been made to the directory %s.\n", directoryName);
    }
  }

  if(close(statFileDescriptor) == -1) {
    perror("Error closing stat file.\n");
    exit(EXIT_FAILURE);
  }

}
