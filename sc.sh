#!/bin/bash

chmod +r "$1"

pattern_1="corrupted"
pattern_2="dangerous"
pattern_3="risk"
pattern_4="attack"
pattern_5="malware"
pattern_6="malicious"

result=0


if grep -q "$pattern_1" "$1"; then
    result=1
fi


if grep -q "$pattern_2" "$1"; then
    result=1
fi


if grep -q "$pattern_3" "$1"; then
    result=1
fi


if grep -q "$pattern_4" "$1"; then
    result=1
fi


if grep -q "$pattern_5" "$1"; then
    result=1
fi


if grep -q "$pattern_6" "$1"; then
    result=1
fi

i=0
lines=0
words=0
chars=0
for entity in $(wc "$1")
do
    if test "$i" -eq 0
    then
        lines=$entity
        i=1
    elif test "$i" -eq 1
    then
        words=$entity
        i=2
    elif test "$i" -eq 2
    then
        chars=$entity
        i=3
    fi
done

#echo "$lines"
#echo "$words"
#echo "$chars"

if test "$lines" -le 1 -o "$lines" -ge 50
then
    result=1
fi

if test "$words" -le 10 -o "$words" -ge 3000
then
    result=1
fi

if test "$chars" -le 25 -o "$chars" -ge 20000
then
    result=1
fi



if test "$lines" -lt 3 -a "$words" -gt 1000 -a "$chars" -gt 2000
then
    result=1
fi

#if grep -q "[^[:ascii:]]" "$1"; then
#    result=1
#fi

if test "$result" -eq 1
then
  echo "MOVE"
else
  echo "SAFE"
fi

chmod -r "$1"
exit 0
